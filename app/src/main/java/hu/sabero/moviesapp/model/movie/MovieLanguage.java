package hu.sabero.moviesapp.model.movie;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

import hu.sabero.moviesapp.model.Language;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "MovieLanguage", id = BaseColumns._ID)
public class MovieLanguage extends Model implements Serializable {

    @Column(name = "Movie", onDelete = Column.ForeignKeyAction.CASCADE)
    private Movie movie;

    @Column(name = "Language", onDelete = Column.ForeignKeyAction.CASCADE)
    private Language language;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}