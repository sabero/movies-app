package hu.sabero.moviesapp.model.tv;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;


/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "TVShowNetwork", id = BaseColumns._ID)
public class TVShowNetwork extends Model implements Serializable {

    @Column(name = "TVShow", onDelete = Column.ForeignKeyAction.CASCADE)
    private TVShow tvShow;

    @Column(name = "Network", onDelete = Column.ForeignKeyAction.CASCADE)
    private Network network;

    public TVShow getTVShow() {
        return tvShow;
    }

    public void setTVShow(TVShow tvShow) {
        this.tvShow = tvShow;
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }
}