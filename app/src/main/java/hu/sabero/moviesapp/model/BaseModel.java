package hu.sabero.moviesapp.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import java.io.Serializable;
import java.math.BigDecimal;

import hu.sabero.moviesapp.utils.UtilMethods;

/**
 * Created by saber on 2016. 10. 21..
 */

public abstract class BaseModel extends Model implements Serializable {

    // This is the unique id given by the server
    @Column(name = "remote_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public long remoteId;

    @Column(name = "backdrop_path")
    private String backdrop_path;

    @Column(name = "homepage")
    private String homepage;

    @Column(name = "original_language")
    private String original_language;

    @Column(name = "overview")
    private String overview;

    @Column(name = "popularity")
    private BigDecimal popularity;

    @Column(name = "poster_path")
    private String poster_path;

    @Column(name = "status")
    private String status;

    @Column(name = "vote_average")
    private double vote_average;

    @Column(name = "vote_count")
    private BigDecimal vote_count;


    public long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(long remoteId) {
        this.remoteId = remoteId;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        if (!UtilMethods.empty(backdrop_path)) this.backdrop_path = backdrop_path;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        if (!UtilMethods.empty(homepage)) this.homepage = homepage;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        if (!UtilMethods.empty(original_language)) this.original_language = original_language;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        if (!UtilMethods.empty(overview)) this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        if (!UtilMethods.empty(poster_path)) this.poster_path = poster_path;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        if (!UtilMethods.empty(status)) this.status = status;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public BigDecimal getVote_count() {
        return vote_count;
    }

    public void setVote_count(BigDecimal vote_count) {
        this.vote_count = vote_count;
    }

}
