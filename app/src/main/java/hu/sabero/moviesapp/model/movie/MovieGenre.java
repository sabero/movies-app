package hu.sabero.moviesapp.model.movie;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

import hu.sabero.moviesapp.model.Genre;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "MovieGenre", id = BaseColumns._ID)
public class MovieGenre extends Model implements Serializable{

    @Column(name = "Movie", onDelete = Column.ForeignKeyAction.CASCADE)
    private Movie movie;

    @Column(name = "Genre", onDelete = Column.ForeignKeyAction.CASCADE)
    private Genre genre;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
