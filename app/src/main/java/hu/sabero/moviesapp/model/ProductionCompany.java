package hu.sabero.moviesapp.model;

        import android.provider.BaseColumns;

        import com.activeandroid.Model;
        import com.activeandroid.annotation.Column;
        import com.activeandroid.annotation.Table;

        import java.io.Serializable;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "ProductionCompanies", id = BaseColumns._ID)
public class ProductionCompany extends Model implements Serializable {

    // This is the unique id given by the server
    @Column(name = "remote_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private long remoteId;

    @Column(name = "name")
    private String name;

    public long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(long remoteId) {
        this.remoteId = remoteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}