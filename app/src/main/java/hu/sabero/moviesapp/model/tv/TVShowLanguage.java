package hu.sabero.moviesapp.model.tv;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

import hu.sabero.moviesapp.model.Language;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "TVShowLanguage", id = BaseColumns._ID)
public class TVShowLanguage extends Model implements Serializable {

    @Column(name = "TVShow", onDelete = Column.ForeignKeyAction.CASCADE)
    private TVShow tvShow;

    @Column(name = "Language", onDelete = Column.ForeignKeyAction.CASCADE)
    private Language language;

    public TVShow getTVShow() {
        return tvShow;
    }

    public void setTVShow(TVShow tvShow) {
        this.tvShow = tvShow;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

}
