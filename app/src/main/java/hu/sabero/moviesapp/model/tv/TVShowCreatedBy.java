package hu.sabero.moviesapp.model.tv;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

import hu.sabero.moviesapp.model.people.Person;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "TVShowCreatedBy", id = BaseColumns._ID)
public class TVShowCreatedBy extends Model implements Serializable {

    @Column(name = "TVShow", onDelete = Column.ForeignKeyAction.CASCADE)
    private TVShow tvShow;

    @Column(name = "Person", onDelete = Column.ForeignKeyAction.CASCADE)
    private Person person;

    public TVShow getTvShow() {
        return tvShow;
    }

    public void setTvShow(TVShow tvShow) {
        this.tvShow = tvShow;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
