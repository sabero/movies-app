package hu.sabero.moviesapp.model.tv;

import android.provider.BaseColumns;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import hu.sabero.moviesapp.model.BaseModel;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.utils.UtilMethods;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "TVShow", id = BaseColumns._ID)
public class TVShow extends BaseModel implements Serializable {

    @Column(name = "first_air_date")
    private Date first_air_date;

    @Column(name = "in_production")
    private boolean in_production;

    @Column(name = "last_air_date")
    private Date last_air_date;

    @Column(name = "name")
    private String name;

    @Column(name = "number_of_episodes")
    private int number_of_episodes;

    @Column(name = "number_of_seasons")
    private int number_of_seasons;

    @Column(name = "original_name")
    private String original_name;

    @Column(name = "type")
    private String type;


    public TVShow(){
        super();
    }

    public Date getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(Date first_air_date) {
        this.first_air_date = first_air_date;
    }

    public boolean isIn_production() {
        return in_production;
    }

    public void setIn_production(boolean in_production) {
        this.in_production = in_production;
    }

    public Date getLast_air_date() {
        return last_air_date;
    }

    public void setLast_air_date(Date last_air_date) {
        this.last_air_date = last_air_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!UtilMethods.empty(name)) this.name = name;
    }

    public int getNumber_of_episodes() {
        return number_of_episodes;
    }

    public void setNumber_of_episodes(int number_of_episodes) {
        this.number_of_episodes = number_of_episodes;
    }

    public int getNumber_of_seasons() {
        return number_of_seasons;
    }

    public void setNumber_of_seasons(int number_of_seasons) {
        this.number_of_seasons = number_of_seasons;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        if (!UtilMethods.empty(original_name)) this.original_name = original_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (!UtilMethods.empty(type)) this.type = type;
    }

    /**
     * Finds existing TV show based on remoteId or creates new TV show and returns
     */
    public static void CreateOrUpdateFromJson(JSONObject jsonObject) {

        try {
            long remoteId = jsonObject.getLong(Constants.REMOTEID);
            TVShow existingTVShow =
                    new Select().from(TVShow.class).where("remote_id = ?", remoteId).executeSingle();
            if (existingTVShow != null) {
                // found and update existing
                TVShow.updateFromJSON(existingTVShow, jsonObject);
            } else {
                // create new TV show
                TVShow.createFromJSON(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create new TV show from JSON
     * @param jsonObject
     */
    public static  void createFromJSON(JSONObject jsonObject){
        TVShow tvShow = new TVShow();
        setTVShowAttributes(tvShow, jsonObject);
    }

    /**
     * Update TV show from JSON
     * @param jsonObject
     */
    public static  void updateFromJSON(TVShow tvShow, JSONObject jsonObject){
        setTVShowAttributes(tvShow, jsonObject);
    }

    /**
     * Modify TV show attributes from JSON and save modifications
     * @param jsonObject
     */
    public static  void setTVShowAttributes(TVShow tvShow, JSONObject jsonObject){
        tvShow.setBackdrop_path(jsonObject.optString(Constants.BACKDROP_PATH));
        if(!UtilMethods.empty(jsonObject.optString(Constants.FIRST_AIR_DATE)))
        tvShow.setFirst_air_date(UtilMethods.convertStringToDate(jsonObject.optString(Constants.FIRST_AIR_DATE)));
        tvShow.setHomepage(jsonObject.optString(Constants.HOMEPAGE));
        tvShow.setIn_production(jsonObject.optBoolean(Constants.IN_PRODUCTION));
        if(!UtilMethods.empty(jsonObject.optString(Constants.LAST_AIR_DATE)))
        tvShow.setLast_air_date(UtilMethods.convertStringToDate(jsonObject.optString(Constants.LAST_AIR_DATE)));
        tvShow.setName(jsonObject.optString(Constants.NAME));
        tvShow.setNumber_of_episodes(jsonObject.optInt(Constants.NUMBER_OF_EPISODES));
        tvShow.setNumber_of_seasons(jsonObject.optInt(Constants.NUMBER_OF_SEASONS));
        tvShow.setOriginal_language(jsonObject.optString(Constants.ORIGINAL_LANGUAGE));
        tvShow.setOriginal_name(jsonObject.optString(Constants.ORIGINAL_NAME));
        tvShow.setOverview(jsonObject.optString(Constants.OVERVIEW));
        tvShow.setPoster_path(jsonObject.optString(Constants.POSTER_PATH));
        tvShow.setRemoteId(jsonObject.optLong(Constants.REMOTEID));
        tvShow.setStatus(jsonObject.optString(Constants.STATUS));
        tvShow.setType(jsonObject.optString(Constants.TYPE));
        tvShow.setVote_average(jsonObject.optDouble(Constants.VOTE_AVERAGE));
        tvShow.setVote_count(BigDecimal.valueOf(jsonObject.optInt(Constants.VOTE_COUNT)));
        tvShow.save();
    }
}
