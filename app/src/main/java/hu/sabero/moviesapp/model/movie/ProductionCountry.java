package hu.sabero.moviesapp.model.movie;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "ProductionCountries", id = BaseColumns._ID)
public class ProductionCountry extends Model implements Serializable {

    // This is the unique id given by the server
    @Column(name = "iso_3166_1", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private long iso_3166_1;

    @Column(name = "name")
    private String name;

    public long getIso_3166_1() {
        return iso_3166_1;
    }

    public void setIso_3166_1(long iso_3166_1) {
        this.iso_3166_1 = iso_3166_1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}