package hu.sabero.moviesapp.model.tv;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

import hu.sabero.moviesapp.model.Genre;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "MovieGenre", id = BaseColumns._ID)
public class TVShowGenre extends Model implements Serializable {

    @Column(name = "TVShow", onDelete = Column.ForeignKeyAction.CASCADE)
    private TVShow tvShow;

    @Column(name = "Genre", onDelete = Column.ForeignKeyAction.CASCADE)
    private Genre genre;

    public TVShow getTVShow() {
        return tvShow;
    }

    public void setTVShow(TVShow tvShow) {
        this.tvShow = tvShow;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}