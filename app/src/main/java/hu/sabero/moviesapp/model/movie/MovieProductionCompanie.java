package hu.sabero.moviesapp.model.movie;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

import hu.sabero.moviesapp.model.ProductionCompany;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "MovieProductionCompanie", id = BaseColumns._ID)
public class MovieProductionCompanie extends Model implements Serializable{

    @Column(name = "Movie", onDelete = Column.ForeignKeyAction.CASCADE)
    private Movie movie;

    @Column(name = "ProductionCompany", onDelete = Column.ForeignKeyAction.CASCADE)
    private ProductionCompany productionCompany;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public ProductionCompany getProductionCompany() {
        return productionCompany;
    }

    public void setProductionCompany(ProductionCompany productionCompany) {
        this.productionCompany = productionCompany;
    }
}