package hu.sabero.moviesapp.model;

        import android.provider.BaseColumns;

        import com.activeandroid.Model;
        import com.activeandroid.annotation.Column;
        import com.activeandroid.annotation.Table;

        import java.io.Serializable;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "Language", id = BaseColumns._ID)
public class Language extends Model implements Serializable {

    // This is the unique id given by the server
    @Column(name = "iso_639_1", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private long iso_639_1;

    @Column(name = "name")
    private String name;

    public long getIso_639_1() {
        return iso_639_1;
    }

    public void setIso_639_1(long iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
