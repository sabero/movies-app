package hu.sabero.moviesapp.model.movie;

import android.provider.BaseColumns;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import hu.sabero.moviesapp.model.BaseModel;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.utils.UtilMethods;

/**
 * Created by saber on 2016. 10. 20..
 */

@Table(name = "Movies", id = BaseColumns._ID)
public class Movie extends BaseModel implements Serializable {

    @Column(name = "adult")
    private boolean adult;

    @Column(name = "budget")
    private BigDecimal budget;

    @Column(name = "imdb_id")
    private String imdb_id;

    @Column(name = "original_title")
    private String original_title;

    @Column(name = "release_date")
    private Date release_date;

    @Column(name = "revenue")
    private BigDecimal revenue;

    @Column(name = "runtime")
    private int runtime;

    @Column(name = "tagline")
    private String tagline;

    @Column(name = "title")
    private String title;

    @Column(name = "video")
    private boolean video;


    public Movie(){
        super();
    }

    public long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(long remoteId) {
        this.remoteId = remoteId;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        if (!UtilMethods.empty(imdb_id)) this.imdb_id = imdb_id;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        if (!UtilMethods.empty(original_title)) this.original_title = original_title;
    }

    public Date getRelease_date() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        if (!UtilMethods.empty(tagline)) this.tagline = tagline;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (!UtilMethods.empty(title)) this.title = title;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    /**
     * Finds existing movie based on remoteId or creates new movie and returns
     */
    public static void CreateOrUpdateFromJson(JSONObject jsonObject) {

        try {
            long remoteId = jsonObject.getLong(Constants.REMOTEID);
            Movie existingMovie =
                    new Select().from(Movie.class).where("remote_id = ?", remoteId).executeSingle();
            if (existingMovie != null) {
                // found and update existing
                Movie.updateFromJSON(existingMovie, jsonObject);
            } else {
                // create new movie
                 Movie.createFromJSON(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create new movie from JSON
     * @param jsonObject
     */
    public static  void createFromJSON(JSONObject jsonObject){
        Movie movie = new Movie();
        setMovieAttributes(movie, jsonObject);
    }

    /**
     * Update movie from JSON
     * @param jsonObject
     */
    public static  void updateFromJSON(Movie movie, JSONObject jsonObject){
        setMovieAttributes(movie, jsonObject);
    }

    /**
     * Modify movie attributes from JSON and save modifications
     * @param jsonObject
     */
    public static  void setMovieAttributes(Movie movie, JSONObject jsonObject){
        movie.setAdult(jsonObject.optBoolean(Constants.ADULT));
        movie.setBackdrop_path(jsonObject.optString(Constants.BACKDROP_PATH));
        movie.setBudget(BigDecimal.valueOf(jsonObject.optInt(Constants.BUDGET)));
        movie.setHomepage(jsonObject.optString(Constants.HOMEPAGE));
        movie.setImdb_id(jsonObject.optString(Constants.IMDB_ID));
        movie.setOriginal_language(jsonObject.optString(Constants.ORIGINAL_LANGUAGE));
        movie.setOriginal_title(jsonObject.optString(Constants.ORIGINAL_TITLE));
        movie.setOverview(jsonObject.optString(Constants.OVERVIEW));
        movie.setPoster_path(jsonObject.optString(Constants.POSTER_PATH));
        if(!UtilMethods.empty(jsonObject.optString(Constants.RELEASE_DATE)))
        movie.setRelease_date(UtilMethods.convertStringToDate(jsonObject.optString(Constants.RELEASE_DATE)));
        movie.setRemoteId(jsonObject.optLong(Constants.REMOTEID));
        movie.setRevenue(BigDecimal.valueOf(jsonObject.optInt(Constants.REVENUE)));
        movie.setRuntime(jsonObject.optInt(Constants.RUNTIME));
        movie.setStatus(jsonObject.optString(Constants.STATUS));
        movie.setTagline(jsonObject.optString(Constants.TAGLINE));
        movie.setTitle(jsonObject.optString(Constants.TITLE));
        movie.setVideo(jsonObject.optBoolean(Constants.VIDEO));
        movie.setVote_average(jsonObject.optDouble(Constants.VOTE_AVERAGE));
        movie.setVote_count(BigDecimal.valueOf(jsonObject.optInt(Constants.VOTE_COUNT)));
        movie.save();
    }

}
