package hu.sabero.moviesapp.model.people;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.utils.UtilMethods;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "People", id = BaseColumns._ID)
public class Person extends Model implements Serializable {

    // This is the unique id given by the server
    @Column(name = "remote_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private long remote_id;

    @Column(name = "adult")
    private boolean adult;

    @Column(name = "biography")
    private String biography;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "deathday")
    private Date deathday;

    @Column(name = "gender")
    private int gender;

    @Column(name = "homepage")
    private String homepage;

    @Column(name = "imdb_id")
    private String imdb_id;

    @Column(name = "name")
    private String name;

    @Column(name = "place_of_birth")
    private String place_of_birth;

    @Column(name = "popularity")
    private BigDecimal popularity;

    @Column(name = "profile_path")
    private String profile_path;

    public Person(){
        super();
    }

    public long getRemoteId() {
        return remote_id;
    }

    public void setRemoteId(long remote_id) {
        this.remote_id = remote_id;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        if (!UtilMethods.empty(biography)) this.biography = biography;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getDeathday() {
        return deathday;
    }

    public void setDeathday(Date deathday) {
        this.deathday = deathday;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        if (!UtilMethods.empty(homepage)) this.homepage = homepage;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        if (!UtilMethods.empty(imdb_id)) this.imdb_id = imdb_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!UtilMethods.empty(name)) this.name = name;
    }

    public String getPlace_of_birth() {
        return place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        if (!UtilMethods.empty(place_of_birth)) this.place_of_birth = place_of_birth;
    }

    public BigDecimal getPopularity() {
        return popularity;
    }

    public void setPopularity(BigDecimal popularity) {
        this.popularity = popularity;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        if (!UtilMethods.empty(profile_path)) this.profile_path = profile_path;
    }

    /**
     * Finds existing movie based on remoteId or creates new person and returns
     */
    public static void CreateOrUpdateFromJson(JSONObject jsonObject) {

        try {
            long remoteId = jsonObject.getLong(Constants.REMOTEID);
            Person existingPerson =
                    new Select().from(Person.class).where("remote_id = ?", remoteId).executeSingle();
            if (existingPerson != null) {
                // found and update existing
                Person.updateFromJSON(existingPerson, jsonObject);
            } else {
                // create new person
                Person.createFromJSON(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create new person from JSON
     * @param jsonObject
     */
    public static  void createFromJSON(JSONObject jsonObject){
        Person person = new Person();
        setPersonAttributes(person, jsonObject);
    }

    /**
     * Update person from JSON
     * @param jsonObject
     */
    public static  void updateFromJSON(Person person, JSONObject jsonObject){
        setPersonAttributes(person, jsonObject);
    }

    /**
     * Modify person attributes from JSON and save modifications
     * @param jsonObject
     */
    public static  void setPersonAttributes(Person person, JSONObject jsonObject){
        person.setAdult(jsonObject.optBoolean(Constants.ADULT));
        person.setBiography(jsonObject.optString(Constants.BIOGRAPHY));
        if(!UtilMethods.empty(jsonObject.optString(Constants.BIRTHDAY)))
        person.setBirthday(UtilMethods.convertStringToDate(jsonObject.optString(Constants.BIRTHDAY)));
        if(!UtilMethods.empty(jsonObject.optString(Constants.DEATHDAY)))
        person.setDeathday(UtilMethods.convertStringToDate(jsonObject.optString(Constants.DEATHDAY)));
        person.setGender(jsonObject.optInt(Constants.GENDER));
        person.setHomepage(jsonObject.optString(Constants.HOMEPAGE));
        person.setImdb_id(jsonObject.optString(Constants.IMDB_ID));
        person.setName(jsonObject.optString(Constants.NAME));
        person.setPlace_of_birth(jsonObject.optString(Constants.PLACE_OF_BIRTH));
        person.setPopularity(BigDecimal.valueOf(jsonObject.optInt(Constants.POPULARITY)));
        person.setProfile_path(jsonObject.optString(Constants.PROFILE_PATH));
        person.setRemoteId(jsonObject.optLong(Constants.REMOTEID));
        person.save();
    }
}
