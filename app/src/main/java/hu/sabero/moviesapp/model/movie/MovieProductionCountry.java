package hu.sabero.moviesapp.model.movie;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "MovieProductionCompanie", id = BaseColumns._ID)
public class MovieProductionCountry extends Model implements Serializable{

    @Column(name = "Movie", onDelete = Column.ForeignKeyAction.CASCADE)
    private Movie movie;

    @Column(name = "ProductionCountry", onDelete = Column.ForeignKeyAction.CASCADE)
    private ProductionCountry productionCountry;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public ProductionCountry getProductionCountry() {
        return productionCountry;
    }

    public void setProductionCountry(ProductionCountry productionCountry) {
        this.productionCountry = productionCountry;
    }
}
