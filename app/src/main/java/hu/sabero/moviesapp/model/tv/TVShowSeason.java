package hu.sabero.moviesapp.model.tv;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;


/**
 * Created by saber on 2016. 10. 21..
 */

@Table(name = "TVShowSeason", id = BaseColumns._ID)
public class TVShowSeason extends Model implements Serializable {

    @Column(name = "TVShow", onDelete = Column.ForeignKeyAction.CASCADE)
    private TVShow tvShow;

    @Column(name = "Season", onDelete = Column.ForeignKeyAction.CASCADE)
    private Season season;

    public TVShow getTVShow() {
        return tvShow;
    }

    public void setTVShow(TVShow tvShow) {
        this.tvShow = tvShow;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }
}