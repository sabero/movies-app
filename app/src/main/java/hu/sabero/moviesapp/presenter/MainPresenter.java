package hu.sabero.moviesapp.presenter;

import android.content.Context;
import android.content.Intent;

import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.view.DetailsActivity;
import hu.sabero.moviesapp.view.MainActivity;

/**
 * Created by saber on 2016. 10. 23..
 */

public class MainPresenter {

    public void navigateToDetailsScreen(Context context, int category, long id) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(Constants.CATEGORY, category);
        intent.putExtra(Constants.REMOTEID, id);
        context.startActivity(intent);
    }
}
