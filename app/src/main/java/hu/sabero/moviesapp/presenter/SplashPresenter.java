package hu.sabero.moviesapp.presenter;

import android.content.Context;
import android.content.Intent;

import hu.sabero.moviesapp.view.MainActivity;

/**
 * Created by saber on 2016. 10. 23..
 */

public class SplashPresenter {

    public void navigateToMainScreen(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }
}
