package hu.sabero.moviesapp.utils;

/**
 * Created by saber on 2016. 10. 21..
 */

public class Constants {

    // Constants for web connections
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 100000;
    public static final int STATUS_ERROR = 400;
    public static final int STATUS_UNAUTHORIZED = 401;

    // API key to access the API
    public static final String API_KEY = "api_key";

    // URL's to be used to access the API
    public static final String END_POINT = "https://api.themoviedb.org/3/";

    public static final String MOVIES_DB_URL = END_POINT + "movie/";
    public static final String POPULAR_MOVIES_DB_URL = END_POINT + "movie/popular";
    public static final String SEARCH_MOVIES_DB_URL =  END_POINT + "search/movie";

    public static final String TV_SHOWS_DB_URL = END_POINT + "tv/";
    public static final String POPULAR_TV_SHOWS_DB_URL = END_POINT + "tv/popular";
    public static final String SEARCH_TV_DB_URL =  END_POINT + "search/tv";

    public static final String PEOPLE_DB_URL = END_POINT + "person/";
    public static final String POPULAR_PEOPLE_DB_URL = END_POINT + "person/popular";
    public static final String SEARCH_PEOPLE_DB_URL =  END_POINT + "search/person";

    public static final String IMAGE_DB_URL = "http://image.tmdb.org/t/p/w185/";

    // Constants used in JSON Parsing or values attached in a URL server connection
    public static final String ADULT = "adult";
    public static final String AIR_DATE = "air_date";
    public static final String BACKDROP_PATH = "backdrop_path";
    public static final String BIOGRAPHY = "biography";
    public static final String BIRTHDAY = "birthday";
    public static final String BUDGET = "budget";
    public static final String CATEGORY = "category";
    public static final String DEATHDAY = "deathday";
    public static final String EPISODE_COUNT = "episode_count";
    public static final String FIRST_AIR_DATE = "first_air_date";
    public static final String GENDER = "gender";
    public static final String HOMEPAGE = "homepage";
    public static final String IMDB_ID = "imdb_id";
    public static final String IN_PRODUCTION = "in_production";
    public static final String ISO_3166_1 = "iso_3166_1";
    public static final String ISO_639_1 = "iso_639_1";
    public static final String LAST_AIR_DATE = "last_air_date";
    public static final String LANGUAGE = "language";
    public static final String LANGUAGES = "languages";
    public static final String NAME = "name";
    public static final String NUMBER_OF_EPISODES = "number_of_episodes";
    public static final String NUMBER_OF_SEASONS = "number_of_seasons";
    public static final String ORIGINAL_LANGUAGE = "original_language";
    public static final String ORIGINAL_NAME = "original_name";
    public static final String ORIGINAL_TITLE = "original_title";
    public static final String OVERVIEW = "overview";
    public static final String PLACE_OF_BIRTH = "place_of_birth";
    public static final String POPULARITY = "popularity";
    public static final String POSTER_PATH = "poster_path";
    public static final String PROFILE_PATH = "profile_path";
    public static final String QUERY = "query";
    public static final String RELEASE_DATE = "release_date";
    public static final String REMOTEID = "id";
    public static final String RESULTS = "results";
    public static final String REVENUE = "revenue";
    public static final String RUNTIME = "runtime";
    public static final String SEASON_NUMBER = "season_number";
    public static final String SPOKEN_LANGUAGES = "spoken_languages";
    public static final String STATUS = "status";
    public static final String TAGLINE = "tagline";
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    public static final String VIDEO = "video";
    public static final String VOTE_AVERAGE = "vote_average";
    public static final String VOTE_COUNT = "vote_count";

    //Messages
    public static final String MESSAGE = "msg";
    public static final String CONNECTION_MESSAGE = "No Internet Connection!";

}
