package hu.sabero.moviesapp.utils;

import com.activeandroid.query.Select;

import java.util.List;

import hu.sabero.moviesapp.model.Genre;
import hu.sabero.moviesapp.model.movie.Movie;
import hu.sabero.moviesapp.model.movie.MovieGenre;

/**
 * Created by saber on 2016. 10. 22..
 */

public class DBHelper {

    /**
     * Method to get a movie related genres
     *
     * @param movie
     * @return
     */
    public List<Genre> getRelatedGenres(Movie movie) {
        List<Genre> genres = null;

        if (movie != null) {
            genres = new Select()
                    .from(Genre.class)
                    .innerJoin(MovieGenre.class).on("MovieGenre.Genre = Genre.id")
                    .where("MovieGenre.Movie = ?", movie.getId())
                    .execute();
        }
        return genres;
    }

    /**
     * Method to get MovieGenre relation
     *
     * @param movie
     * @param genre
     * @return
     */
    public MovieGenre getMovieGenre(Movie movie, Genre genre) {
        MovieGenre movieGenre = null;
        if (movie != null && genre != null) {

            movieGenre = new Select().from(MovieGenre.class)
                    .where("Movie = ?", movie.getId())
                    .where("Genre = ?", genre.getId())
                    .executeSingle();
        }
        return movieGenre;
    }

    /**
     * Method to add MovieGenre relation
     *
     * @param movie
     * @param genre
     * @return
     */
    public MovieGenre addClientContract(Movie movie, Genre genre) {
        MovieGenre movieGenre = getMovieGenre(movie, genre);

        if (movie != null && genre != null && movieGenre == null) {

            if (movie.getId() == null)
                movie.save();

            if (genre.getId() == null)
                genre.save();

            movieGenre = new MovieGenre();
            movieGenre.setMovie(movie);
            movieGenre.setGenre(genre);
            movieGenre.save();

        }
        return movieGenre;
    }

    /**
     * Methode to delete MovieGenre relation
     *
     * @param movie
     * @param genre
     */
    public void deleteMovieGenre(Movie movie, Genre genre) {
        MovieGenre movieGenre = getMovieGenre(movie, genre);
        if (movieGenre != null)
            movieGenre.delete();
    }
}
