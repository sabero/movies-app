package hu.sabero.moviesapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import hu.sabero.moviesapp.R;

/**
 * Created by saber on 2016. 10. 22..
 */

public class UtilMethods {

    public static Date convertStringToDate(String dateString){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getYear(Date date){
        if(date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return Integer.toString(cal.get(Calendar.YEAR));
        }else{
            return "n/a";
        }
    }

    public static boolean empty( final String string ) {
        return string == null || string.trim().isEmpty();
    }

    public static boolean checkNetwork(Context context, View view){
        if(!UtilMethods.isConnectedToInternet(context)){
            Snackbar.make(view, context.getString(R.string.no_network), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    /**
     * @param context
     * @return true or false mentioning the device is connected or not
     * @brief checking the internet connection on run time
     */
    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
