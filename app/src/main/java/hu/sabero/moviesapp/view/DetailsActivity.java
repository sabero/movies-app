package hu.sabero.moviesapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.utils.Constants;

/**
 * Created by saber on 2016. 10. 24..
 */

public class DetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //opening transition animations
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_details);

        int category = 0;
        long id = 0;

        Intent intent = getIntent();
        if(intent != null) {
            category = intent.getIntExtra(Constants.CATEGORY, 0);
            id = intent.getLongExtra(Constants.REMOTEID, 0);
        }

        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putInt(Constants.CATEGORY, category);
            arguments.putLong(Constants.REMOTEID, id);
            PosterFragment fragment = new PosterFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.detail_container, fragment)
                    .commit();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //closing transition animations
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

}
