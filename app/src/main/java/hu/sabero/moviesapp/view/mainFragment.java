package hu.sabero.moviesapp.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.presenter.MainPresenter;
import hu.sabero.moviesapp.utils.UtilMethods;
import hu.sabero.moviesapp.webservices.PopularMoviesService;
import hu.sabero.moviesapp.webservices.PopularPeopleService;
import hu.sabero.moviesapp.webservices.PopularTVShowsService;
import hu.sabero.moviesapp.webservices.SearchMoviesService;
import hu.sabero.moviesapp.webservices.SearchPeopleService;
import hu.sabero.moviesapp.webservices.SearchTVShowsService;

/**
 * Created by saber on 2016. 10. 22..
 */

public class MainFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_CATEGORY_NUMBER = "category_number";
    public static SwipeRefreshLayout moviesSwipeRefreshLayout;
    public static SwipeRefreshLayout tvShowsSwipeRefreshLayout;
    public static SwipeRefreshLayout peopleSwipeRefreshLayout;
    public static RecyclerView moviesRecyclerView;
    public static RecyclerView tvShowsRecyclerView;
    public static RecyclerView peopleRecyclerView;
    public static String query;
    private int category;
    private View rootView;


    public MainFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainFragment newInstance(int position) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CATEGORY_NUMBER, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        category = getArguments().getInt(ARG_CATEGORY_NUMBER);
        if(category == 1) {
            rootView = inflater.inflate(R.layout.fragment_popular_tvshows, container, false);
            initialize();
        }else if (category == 2){
            rootView = inflater.inflate(R.layout.fragment_popular_people, container, false);
            initialize();
        }else{
            rootView = inflater.inflate(R.layout.fragment_popular_movies, container, false);
            initialize();
        }

        return rootView;
    }

    private void initialize(){

        if(category == 1) {
            tvShowsSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.tvshows_swipe_refresh_layout);
            tvShowsSwipeRefreshLayout.setOnRefreshListener(this);
            tvShowsSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            tvShowsRecyclerView = (RecyclerView) rootView.findViewById(R.id.tvshows_recycler_view);
            tvShowsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            loadPopularTVShows();
        }else if (category == 2){
            peopleSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.people_swipe_refresh_layout);
            peopleSwipeRefreshLayout.setOnRefreshListener(this);
            peopleSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            peopleRecyclerView = (RecyclerView) rootView.findViewById(R.id.people_recycler_view);
            peopleRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            loadPopularPeople();
        }else{
            moviesSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.movies_swipe_refresh_layout);
            moviesSwipeRefreshLayout.setOnRefreshListener(this);
            moviesSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            moviesRecyclerView = (RecyclerView) rootView.findViewById(R.id.movies_recycler_view);
            moviesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            loadPopularMovies();
        }
    }

    private void loadPopularMovies(){
        if(UtilMethods.empty(query)){
            PopularMoviesService movieService = new PopularMoviesService(getContext());
            movieService.execute();
        }else{
            SearchMoviesService searchMoviesService = new SearchMoviesService(getContext());
            searchMoviesService.execute();
        }
    }

    private void loadPopularTVShows(){
        if(UtilMethods.empty(query)){
            PopularTVShowsService popularTVShowsService = new PopularTVShowsService(getContext());
            popularTVShowsService.execute();
        }else {
            SearchTVShowsService searchTVShowsService = new SearchTVShowsService(getContext());
            searchTVShowsService.execute();
        }
    }

    private void loadPopularPeople(){
        if(UtilMethods.empty(query)) {
            PopularPeopleService popularPeopleService = new PopularPeopleService(getContext());
            popularPeopleService.execute();
        }else {
            SearchPeopleService searchMoviesService = new SearchPeopleService(getContext());
            searchMoviesService.execute();
        }
    }

    @Override
    public void onRefresh() {
        Toast.makeText(getContext(), "Refreshing...", Toast.LENGTH_SHORT).show();
        if(category == 1) {
            loadPopularTVShows();
        }else if (category == 2){
            loadPopularPeople();
        }else{
            loadPopularMovies();
        }
    }
}
