package hu.sabero.moviesapp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.squareup.picasso.Picasso;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.model.movie.Movie;
import hu.sabero.moviesapp.model.tv.TVShow;
import hu.sabero.moviesapp.utils.Constants;

/**
 * Created by saber on 2016. 10. 24..
 */

public class PosterFragment extends Fragment {

    private int category;
    private long id;

    public PosterFragment(){
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_poster, container, false);
        ImageView poster = (ImageView) rootView.findViewById(R.id.poster);

        Button details_button = (Button) rootView.findViewById(R.id.details_button);
        details_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "TODO: \nReplace this fragment (PosterFragment) with a DatailsFragment to show details.", Toast.LENGTH_LONG).show();
            }
        });

        if(getArguments().containsKey(Constants.CATEGORY) && getArguments().containsKey(Constants.REMOTEID)){
            category = getArguments().getInt(Constants.CATEGORY);
            id = getArguments().getLong(Constants.REMOTEID);

            if(category == 0){
                Movie selectedMovie = new Select().from(Movie.class).where("remote_id = ?", id).executeSingle();
                if(selectedMovie != null)
                Picasso.with(getContext())
                        .load(Constants.IMAGE_DB_URL + selectedMovie.getPoster_path())
                        .placeholder(R.drawable.placeholder_image)
                        .error(R.drawable.placeholder_image)
                        .into(poster);
            }else {
                TVShow selectedTVShow = new Select().from(TVShow.class).where("remote_id = ?", id).executeSingle();
                if(selectedTVShow != null)
                    Picasso.with(getContext())
                            .load(Constants.IMAGE_DB_URL + selectedTVShow.getPoster_path())
                            .placeholder(R.drawable.placeholder_image)
                            .error(R.drawable.placeholder_image)
                            .into(poster);
            }
        }
        return rootView;
    }
}
