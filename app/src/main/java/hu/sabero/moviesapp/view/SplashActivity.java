package hu.sabero.moviesapp.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.presenter.SplashPresenter;
import hu.sabero.moviesapp.utils.UtilMethods;

/**
 * Created by saber on 2016. 10. 23..
 */

public class SplashActivity extends AppCompatActivity {


    private Animation cotinueButtonAnim;
    private Context context;
    private SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //opening transition animations
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_splash);

        context = this;
        presenter = new SplashPresenter();
        cotinueButtonAnim = AnimationUtils.loadAnimation(this, R.anim.button_animation);
        Button continueButton = (Button) findViewById(R.id.continue_button);
        continueButton.setAnimation(cotinueButtonAnim);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UtilMethods.checkNetwork(context, v)){
                    v.startAnimation(cotinueButtonAnim);
                    presenter.navigateToMainScreen(SplashActivity.this);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        //closing transition animations
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

}
