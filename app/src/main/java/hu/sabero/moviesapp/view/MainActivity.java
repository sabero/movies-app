package hu.sabero.moviesapp.view;

import android.app.SearchManager;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.adapters.CategoryPagerAdapter;
import hu.sabero.moviesapp.presenter.MainPresenter;
import hu.sabero.moviesapp.webservices.SearchMoviesService;
import hu.sabero.moviesapp.webservices.SearchPeopleService;
import hu.sabero.moviesapp.webservices.SearchTVShowsService;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private CategoryPagerAdapter mCategoryPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private List<Fragment> mainFragmentInstances;
    public static MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //opening transition animations
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainPresenter = new MainPresenter();
        mainFragmentInstances = new ArrayList<Fragment>();
        mainFragmentInstances.add(0, MainFragment.newInstance(0));
        mainFragmentInstances.add(1, MainFragment.newInstance(1));
        mainFragmentInstances.add(2, MainFragment.newInstance(2));
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mCategoryPagerAdapter = new CategoryPagerAdapter(getSupportFragmentManager(), mainFragmentInstances);
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mCategoryPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                int category = tabLayout.getSelectedTabPosition();
                if(category == 1){
                    SearchTVShowsService searchTVShowsService = new SearchTVShowsService(MainActivity.this);
                    searchTVShowsService.execute();
                }else if(category == 2){
                    SearchPeopleService searchMoviesService = new SearchPeopleService(MainActivity.this);
                    searchMoviesService.execute();
                }else {
                    SearchMoviesService searchMoviesService = new SearchMoviesService(MainActivity.this);
                    searchMoviesService.execute();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                MainFragment.query = query;
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //closing transition animations
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

}
