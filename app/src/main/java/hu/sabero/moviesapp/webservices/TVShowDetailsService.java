package hu.sabero.moviesapp.webservices;

import android.content.ContentValues;
import android.content.Context;

import org.json.JSONObject;

import java.util.Locale;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.model.tv.TVShow;
import hu.sabero.moviesapp.utils.Constants;

/**
 * Created by saber on 2016. 10. 22..
 */

public class TVShowDetailsService extends WebServiceTask {

    private JSONObject jsonObject;
    private long tv_id;

    public TVShowDetailsService(Context mContext, long tv_id) {
        super(mContext);
        this.tv_id = tv_id;
    }

    @Override
    public void showProgress(){
    }

    @Override
    public boolean performRequest() {
        ContentValues urlValues = new ContentValues();
        urlValues.put(Constants.LANGUAGE, Locale.getDefault().getISO3Language());
        urlValues.put(Constants.API_KEY, mContext.getString(R.string.api_key));
        jsonObject = WebServiceUtils.requestJSONObject(Constants.TV_SHOWS_DB_URL + tv_id ,
                WebServiceUtils.METHOD.GET, urlValues);
        if(!hasError(jsonObject)) {
            return true;
        }
        return false;
    }

    @Override
    public void performSuccessfulOperation() {
        TVShow.CreateOrUpdateFromJson(jsonObject);
    }

    @Override
    public void hideProgress() {
    }

}
