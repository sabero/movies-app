package hu.sabero.moviesapp.webservices;

import android.content.ContentValues;
import android.content.Context;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.adapters.MovieAdapter;
import hu.sabero.moviesapp.model.movie.Movie;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.view.MainFragment;

/**
 * Created by saber on 2016. 10. 22..
 */

public class SearchMoviesService extends WebServiceTask {

    private JSONObject jsonObject;

    public SearchMoviesService(Context mContext) {
        super(mContext);
    }

    @Override
    public void showProgress() {
        MainFragment.moviesSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public boolean performRequest() {
        ContentValues urlValues = new ContentValues();
        urlValues.put(Constants.QUERY, MainFragment.query);
        urlValues.put(Constants.LANGUAGE, Locale.getDefault().getISO3Language());
        urlValues.put(Constants.API_KEY, mContext.getString(R.string.api_key));
        jsonObject = WebServiceUtils.requestJSONObject(Constants.SEARCH_MOVIES_DB_URL,
                WebServiceUtils.METHOD.GET, urlValues);
        if(!hasError(jsonObject)) {
            return true;
        }
        return false;
    }

    @Override
    public void performSuccessfulOperation() {
        new Delete().from(Movie.class).execute();
        JSONArray jsonArray = jsonObject.optJSONArray(Constants.RESULTS);
        for(int i=0; i<jsonArray.length(); i++){
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            Movie.CreateOrUpdateFromJson(jsonObject);
        }
        List<Movie> popularMovies = new Select().all().from(Movie.class).execute();
        MainFragment.moviesRecyclerView.setAdapter(new MovieAdapter(mContext, popularMovies));

    }

    @Override
    public void hideProgress() {
        MainFragment.moviesSwipeRefreshLayout.setRefreshing(false);
    }

}
