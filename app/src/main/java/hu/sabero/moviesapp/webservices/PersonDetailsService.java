package hu.sabero.moviesapp.webservices;

import android.content.ContentValues;
import android.content.Context;

import org.json.JSONObject;

import java.util.Locale;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.model.people.Person;
import hu.sabero.moviesapp.utils.Constants;

/**
 * Created by saber on 2016. 10. 22..
 */

public class PersonDetailsService extends WebServiceTask {

    private JSONObject jsonObject;
    private long person_id;

    public PersonDetailsService(Context mContext, long person_id) {
        super(mContext);
        this.person_id = person_id;
    }

    @Override
    public void showProgress(){
    }

    @Override
    public boolean performRequest() {
        ContentValues urlValues = new ContentValues();
        urlValues.put(Constants.LANGUAGE, Locale.getDefault().getISO3Language());
        urlValues.put(Constants.API_KEY, mContext.getString(R.string.api_key));
        jsonObject = WebServiceUtils.requestJSONObject(Constants.PEOPLE_DB_URL + person_id ,
                WebServiceUtils.METHOD.GET, urlValues);
        if(!hasError(jsonObject)) {
            return true;
        }
        return false;
    }

    @Override
    public void performSuccessfulOperation() {
        Person.CreateOrUpdateFromJson(jsonObject);
    }

    @Override
    public void hideProgress() {
    }

}
