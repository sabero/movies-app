package hu.sabero.moviesapp.webservices;

import android.content.ContentValues;
import android.content.Context;
import org.json.JSONObject;

import java.util.Locale;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.model.movie.Movie;
import hu.sabero.moviesapp.utils.Constants;

/**
 * Created by saber on 2016. 10. 22..
 */

public class MovieDetailsService extends WebServiceTask {

    private JSONObject jsonObject;
    private long movie_id;

    public MovieDetailsService(Context mContext, long movie_id) {
        super(mContext);
        this.movie_id = movie_id;
    }

    @Override
    public void showProgress(){
    }

    @Override
    public boolean performRequest() {
        ContentValues urlValues = new ContentValues();
        urlValues.put(Constants.LANGUAGE, Locale.getDefault().getISO3Language());
        urlValues.put(Constants.API_KEY, mContext.getString(R.string.api_key));
        jsonObject = WebServiceUtils.requestJSONObject(Constants.MOVIES_DB_URL + movie_id ,
                WebServiceUtils.METHOD.GET, urlValues);
        if(!hasError(jsonObject)) {
            return true;
        }
        return false;
    }

    @Override
    public void performSuccessfulOperation() {
        Movie.CreateOrUpdateFromJson(jsonObject);
    }

    @Override
    public void hideProgress() {
    }

}
