package hu.sabero.moviesapp.webservices;

import android.content.ContentValues;
import android.content.Context;
import com.activeandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.adapters.TVShowAdapter;
import hu.sabero.moviesapp.model.tv.TVShow;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.view.MainFragment;

/**
 * Created by saber on 2016. 10. 22..
 */

public class PopularTVShowsService extends WebServiceTask {

    private JSONObject jsonObject;

    public PopularTVShowsService(Context mContext) {
        super(mContext);
    }

    @Override
    public void showProgress() {
        MainFragment.tvShowsSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public boolean performRequest() {
        ContentValues urlValues = new ContentValues();
        urlValues.put(Constants.LANGUAGE, Locale.getDefault().getISO3Language());
        urlValues.put(Constants.API_KEY, mContext.getString(R.string.api_key));
        jsonObject = WebServiceUtils.requestJSONObject(Constants.POPULAR_TV_SHOWS_DB_URL,
                WebServiceUtils.METHOD.GET, urlValues);
        if(!hasError(jsonObject)) {
            return true;
        }
        return false;
    }

    @Override
    public void performSuccessfulOperation() {
        JSONArray jsonArray = jsonObject.optJSONArray(Constants.RESULTS);
        for(int i=0; i<jsonArray.length(); i++){
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            TVShow.CreateOrUpdateFromJson(jsonObject);
        }
        List<TVShow> popularTVShows = new Select().all().from(TVShow.class).execute();
        MainFragment.tvShowsRecyclerView.setAdapter(new TVShowAdapter(mContext, popularTVShows));
    }

    @Override
    public void hideProgress() {
        MainFragment.tvShowsSwipeRefreshLayout.setRefreshing(false);
    }

}
