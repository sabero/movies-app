package hu.sabero.moviesapp.webservices;

import android.content.ContentValues;
import android.content.Context;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.adapters.PeopleAdapter;
import hu.sabero.moviesapp.model.people.Person;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.view.MainFragment;

/**
 * Created by saber on 2016. 10. 22..
 */

public class SearchPeopleService extends WebServiceTask {

    private JSONObject jsonObject;

    public SearchPeopleService(Context mContext) {
        super(mContext);
    }

    @Override
    public void showProgress() {
        MainFragment.peopleSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public boolean performRequest() {
        ContentValues urlValues = new ContentValues();
        urlValues.put(Constants.QUERY, MainFragment.query);
        urlValues.put(Constants.LANGUAGE, Locale.getDefault().getISO3Language());
        urlValues.put(Constants.API_KEY, mContext.getString(R.string.api_key));
        jsonObject = WebServiceUtils.requestJSONObject(Constants.SEARCH_PEOPLE_DB_URL,
                WebServiceUtils.METHOD.GET, urlValues);
        if(!hasError(jsonObject)) {
            return true;
        }
        return false;
    }

    @Override
    public void performSuccessfulOperation() {
        new Delete().from(Person.class).execute();
        JSONArray jsonArray = jsonObject.optJSONArray(Constants.RESULTS);
        for(int i=0; i<jsonArray.length(); i++){
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            Person.CreateOrUpdateFromJson(jsonObject);
        }
        List<Person> popularPeople = new Select().all().from(Person.class).execute();
        MainFragment.peopleRecyclerView.setAdapter(new PeopleAdapter(mContext, popularPeople));

    }

    @Override
    public void hideProgress() {
        MainFragment.peopleSwipeRefreshLayout.setRefreshing(false);
    }

}
