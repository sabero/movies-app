package hu.sabero.moviesapp;

import com.activeandroid.ActiveAndroid;

/**
 * Created by saber on 2016. 10. 20..
 */

public class MoviesApplication extends com.activeandroid.app.Application{

    private static MoviesApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        instance = this;
    }
}
