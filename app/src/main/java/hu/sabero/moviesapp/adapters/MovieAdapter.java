package hu.sabero.moviesapp.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.model.movie.Movie;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.utils.UtilMethods;
import hu.sabero.moviesapp.view.MainActivity;
import hu.sabero.moviesapp.webservices.MovieDetailsService;


public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private List<Movie> popularMovies;
    private Context mContext;

    public MovieAdapter(Context mContext, List<Movie> popularMovies) {
        this.mContext = mContext;
        this.popularMovies = popularMovies;
//        updatePopularMovies();
    }

    private void updatePopularMovies(){
        for(Movie movie: popularMovies){
            MovieDetailsService movieDetailsService = new MovieDetailsService(mContext, movie.getRemoteId());
            movieDetailsService.execute();
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView movieTitle;
        public TextView movieVoteAverage;
        public TextView movieSubtitle;
        public TextView movieReleaseYear;
        public TextView movieOverview;
        public TextView moreInfo;
        public ImageView moviePoster;

        public ViewHolder(View itemView) {
            super(itemView);
            movieTitle = (TextView) itemView.findViewById(R.id.movieTitle);
            movieVoteAverage = (TextView) itemView.findViewById(R.id.movieVoteAverage);
            movieSubtitle = (TextView) itemView.findViewById(R.id.movieSubtitle);
            movieReleaseYear = (TextView) itemView.findViewById(R.id.movieReleaseYear);
            movieOverview = (TextView) itemView.findViewById(R.id.movieOverview);
            moreInfo = (TextView) itemView.findViewById(R.id.moreInfo);
            moviePoster = (ImageView) itemView.findViewById(R.id.moviePoster);
        }
    }

    //This method inflates a layout from XML and returning the holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Inflate the custom layout
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.movie_item,
                        parent,
                        false);

        return new ViewHolder(view);
    }

    // Populates data into the item through holder
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Movie movie = popularMovies.get(position);

        String movieTitleText = movie.getTitle();
        String movieVoteAverageText = Double.toString(movie.getVote_average());
        String movieSubtitleText = movie.getTitle();
        String movieReleaseYearText = UtilMethods.getYear(movie.getRelease_date());
        String movieOverviewText = movie.getOverview();

        if(!UtilMethods.empty(movieTitleText) && movieTitleText.length() > 15) movieTitleText = movieTitleText.substring(0,15) + "...";
        if(!UtilMethods.empty(movieSubtitleText) && movieSubtitleText.length() > 15) movieSubtitleText = movieSubtitleText.substring(0,15) + "...";
        if(!UtilMethods.empty(movieOverviewText) && movieOverviewText.length() > 150) movieOverviewText = movieOverviewText.substring(0,150) + "...";

        holder.movieTitle.setText(movieTitleText);
        holder.movieVoteAverage.setText(movieVoteAverageText);
        holder.movieSubtitle.setText(movieSubtitleText);
        holder.movieReleaseYear.setText(movieReleaseYearText);
        holder.movieOverview.setText(movieOverviewText);
        Picasso.with(mContext)
                .load(Constants.IMAGE_DB_URL + movie.getPoster_path())
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image)
                .into(holder.moviePoster);

        holder.moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.mainPresenter.navigateToDetailsScreen(mContext, 0, movie.getRemoteId());
            }
        });

    }

    @Override
    public int getItemCount() {
        if (popularMovies == null) {
            return 0;
        }
        return popularMovies.size();
    }


}
