package hu.sabero.moviesapp.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.model.people.Person;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.utils.UtilMethods;
import hu.sabero.moviesapp.webservices.PersonDetailsService;


public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {

    private List<Person> popularPeople;
    private Context mContext;

    public PeopleAdapter(Context mContext, List<Person> popularPeople) {
        this.mContext = mContext;
        this.popularPeople = popularPeople;
//        updatePopularPeople();
    }

    private void updatePopularPeople(){
        for(Person person: popularPeople){
            PersonDetailsService personDetailsService = new PersonDetailsService(mContext, person.getRemoteId());
            personDetailsService.execute();
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView personName;
        public TextView personBiography;
        public ImageView personProfileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            personName = (TextView) itemView.findViewById(R.id.personName);
            personBiography = (TextView) itemView.findViewById(R.id.personBiography);
            personProfileImage = (ImageView) itemView.findViewById(R.id.personProfileImage);
        }
    }

    //This method inflates a layout from XML and returning the holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Inflate the custom layout
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.person_item,
                        parent,
                        false);

        return new ViewHolder(view);
    }

    // Populates data into the item through holder
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Person person = popularPeople.get(position);

        String personNameText = person.getName();
        String personBiographyText = person.getBiography();

        if(!UtilMethods.empty(personNameText) && personNameText.length() > 20) personNameText = personNameText.substring(0,20) + "...";
        if(!UtilMethods.empty(personBiographyText) && personBiographyText.length() > 200) personBiographyText = personBiographyText.substring(0,200) + "...";

        holder.personName.setText(personNameText);
        holder.personBiography.setText(personBiographyText);
        Picasso.with(mContext)
                .load(Constants.IMAGE_DB_URL + person.getProfile_path())
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image)
                .into(holder.personProfileImage);
    }


    @Override
    public int getItemCount() {
        if (popularPeople == null) {
            return 0;
        }
        return popularPeople.size();
    }


}
