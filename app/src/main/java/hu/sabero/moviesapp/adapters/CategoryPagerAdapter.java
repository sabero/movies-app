package hu.sabero.moviesapp.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;


/** *
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 *
 * Created by saber on 2016. 10. 22..
 */
public class CategoryPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> mainFragmentInstances;

    public CategoryPagerAdapter(FragmentManager fm, List<Fragment> mainFragmentInstances) {
        super(fm);
        this.mainFragmentInstances = mainFragmentInstances;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
           return mainFragmentInstances.get(position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Movies";
            case 1:
                return "TV Shows";
            case 2:
                return "People";
        }
        return null;
    }
}
