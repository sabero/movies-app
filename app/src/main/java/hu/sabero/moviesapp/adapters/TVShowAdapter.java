package hu.sabero.moviesapp.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import hu.sabero.moviesapp.R;
import hu.sabero.moviesapp.model.tv.TVShow;
import hu.sabero.moviesapp.utils.Constants;
import hu.sabero.moviesapp.utils.UtilMethods;
import hu.sabero.moviesapp.view.MainActivity;
import hu.sabero.moviesapp.webservices.TVShowDetailsService;


public class TVShowAdapter extends RecyclerView.Adapter<TVShowAdapter.ViewHolder> {

    private List<TVShow> popularTVShows;
    private Context mContext;

    public TVShowAdapter(Context mContext, List<TVShow> popularTVShows) {
        this.mContext = mContext;
        this.popularTVShows = popularTVShows;
//        updatePopularTVShows();
    }

    private void updatePopularTVShows(){
        for(TVShow tvShow: popularTVShows){
            TVShowDetailsService tvShowDetailsService = new TVShowDetailsService(mContext, tvShow.getRemoteId());
            tvShowDetailsService.execute();
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvShowTitle;
        public TextView tvShowVoteAverage;
        public TextView tvShowSubtitle;
        public TextView tvShowReleaseYear;
        public TextView tvShowOverview;
        public TextView moreInfo;
        public ImageView tvShowPoster;

        public ViewHolder(View itemView) {
            super(itemView);
            tvShowTitle = (TextView) itemView.findViewById(R.id.tvShowTitle);
            tvShowVoteAverage = (TextView) itemView.findViewById(R.id.tvShowVoteAverage);
            tvShowSubtitle = (TextView) itemView.findViewById(R.id.tvShowSubtitle);
            tvShowReleaseYear = (TextView) itemView.findViewById(R.id.tvShowReleaseYear);
            tvShowOverview = (TextView) itemView.findViewById(R.id.tvShowOverview);
            moreInfo = (TextView) itemView.findViewById(R.id.moreInfo);
            tvShowPoster = (ImageView) itemView.findViewById(R.id.tvShowPoster);
        }
    }

    //This method inflates a layout from XML and returning the holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Inflate the custom layout
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.tvshow_item,
                        parent,
                        false);

        return new ViewHolder(view);
    }

    // Populates data into the item through holder
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TVShow tvShow = popularTVShows.get(position);

        String tvShowTitleText = tvShow.getName();
        String tvShowVoteAverageText = Double.toString(tvShow.getVote_average());
        String tvShowSubtitleText = tvShow.getName();
        String tvShowReleaseYearText = UtilMethods.getYear(tvShow.getFirst_air_date());
        String tvShowOverviewText = tvShow.getOverview();

        if(tvShowTitleText.length() > 15) tvShowTitleText = tvShowTitleText.substring(0,15) + "...";
        if(tvShowSubtitleText.length() > 15) tvShowSubtitleText = tvShowSubtitleText.substring(0,15) + "...";
        if(tvShowOverviewText.length() > 150) tvShowOverviewText = tvShowOverviewText.substring(0,150) + "...";

        holder.tvShowTitle.setText(tvShowTitleText);
        holder.tvShowVoteAverage.setText(tvShowVoteAverageText);
        holder.tvShowSubtitle.setText(tvShowSubtitleText);
        holder.tvShowReleaseYear.setText(tvShowReleaseYearText);
        holder.tvShowOverview.setText(tvShowOverviewText);
        Picasso.with(mContext)
                .load(Constants.IMAGE_DB_URL + tvShow.getPoster_path())
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image)
                .into(holder.tvShowPoster);

        holder.moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.mainPresenter.navigateToDetailsScreen(mContext, 1, tvShow.getRemoteId());
            }
        });

    }


    @Override
    public int getItemCount() {
        if (popularTVShows == null) {
            return 0;
        }
        return popularTVShows.size();
    }


}
