# Movies App #


### Task ###

Create demo app that will: - on load fetch the top 20 most popular movies and display it in list - on first load, open the first category (Movies) - use current navigation patterns to display 3 categories - Movies (default), TV Shows and People - allow the user to search within a category




![Screenshot_2016-11-03-17-34-10.png](https://bitbucket.org/repo/6brk7X/images/4033014263-Screenshot_2016-11-03-17-34-10.png)    ![Screenshot_2016-11-03-17-35-30.png](https://bitbucket.org/repo/6brk7X/images/2528187448-Screenshot_2016-11-03-17-35-30.png)

![Screenshot_2016-11-03-17-36-15.png](https://bitbucket.org/repo/6brk7X/images/3498499375-Screenshot_2016-11-03-17-36-15.png)    ![Screenshot_2016-11-03-17-36-26.png](https://bitbucket.org/repo/6brk7X/images/1588323969-Screenshot_2016-11-03-17-36-26.png)

![Screenshot_2016-11-03-17-41-40.png](https://bitbucket.org/repo/6brk7X/images/3690173147-Screenshot_2016-11-03-17-41-40.png)